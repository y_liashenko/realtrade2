$(document).ready(function() {
    /*
     * @build-stuff hide/show
     * */

    var buildStuff = $('.build-stuff');

    $('.more').on('click', function() {
        if ( buildStuff.hasClass('hidden') ) {
            $(buildStuff).removeClass('hidden');
        } else {
            $(buildStuff).addClass('hidden');
        }
    });

    /*
     * @animation
     * */

    var lastWasLower = false,
        quit = false;


    $(window).scroll(function(){
        var windowWidth = $(this).width();
        var windowHeight = $(this).height();
        var windowScrollTop = $(this).scrollTop();
        /*
         * @numbers animation
         * */

        var zoomInadditional = function() {
                $('.additional').find('li').addClass('zoomIn');
                $('.additional').find('li').css('opacity', '1');
            };

        /*
        * @trust-in-block animation
        * */

        if(windowScrollTop>2200 && windowWidth>=1200 || windowScrollTop>2800 && windowWidth>=1000){
                $('.trust-in li:nth-child(1)').find('div').addClass('fadeInLeft');

            if (!lastWasLower){
                $('.begin-odometer').html('2002');
                $('.roofs-odometer').html('300');
                lastWasLower = true;
            } else {
                lastWasLower = false;
            }
                $('.trust-in li:nth-child(2)').find('div').addClass('fadeInRight');
            }
        if(windowScrollTop>2500 && windowWidth>=1200 || windowScrollTop>3300 && windowWidth>=1000){
                $('.trust-in li:nth-child(3)').find('div').addClass('fadeInLeft');
                $('.trust-in li:nth-child(4)').find('div').addClass('fadeInRight');
            } else {
            $('.trust-in li').find('img').css('opacity', '1');
        }
        if(windowScrollTop>2800 && windowWidth>=1200 || windowScrollTop>3600 && windowWidth>=1000){
                $('.trust-in li:nth-child(5)').find('div').addClass('fadeInLeft');
                setTimeout(function(){
                }, 1500);

            if (!quit){
                setTimeout(function(){
                    $('.percent-odometer').html(85);
                }, 1000);
                setTimeout(function(){
                    $('.garantee-odometer').html(3);
                }, 800);
                quit = true;
            } else {
                quit = false;
            }

                $('.trust-in li:nth-child(6)').find('div').addClass('fadeInRight');
                setTimeout(function(){
                }, 200);
            } else {
            $('.trust-in li').find('img').css('opacity', '1');
        }
        /*
         * @services animation
         * */
        if(windowScrollTop>3500 && windowWidth>=1200 || windowScrollTop>4200 && windowWidth>=1000){
            $('.services').find('li:nth-child(-n+3)').addClass('zoomIn');
            $('.services').find('li:nth-child(-n+3)').css('opacity', '1');
        }
        if(windowScrollTop>4000 && windowWidth>=1200 || windowScrollTop>4600 && windowWidth>=1000){
            $('.services').find('li:nth-child(n+4):nth-child(-n+6)').addClass('zoomIn');
            $('.services').find('li:nth-child(n+4):nth-child(-n+6)').css('opacity', '1');
        }
        /*
         * @additional animation
         * */

        if(windowScrollTop>4600 && windowWidth<=2500 || windowScrollTop>7100 && windowWidth<=1200){
            zoomInadditional();
        }
    });

});